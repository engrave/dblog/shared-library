import {tagRegex} from './tagRegex';
import {usernameRegex} from './usernameRegex';

const steem = {
    tagRegex,
    usernameRegex
};

export { steem };
