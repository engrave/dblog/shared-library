const tagRegex = /^(?=.{2,24}$)([[a-z][a-z0-9]*-?[a-z0-9]+)$/;

export { tagRegex }