export class ArticleNotFound extends Error {
    constructor() {
        super("Article not found");
    }
}
