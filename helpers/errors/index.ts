import {ArticleNotFound} from './ArticleNotFound';
import {BlogNotExist} from './BlogNotExist';
import {ValidationError} from './ValidationError';

const errors = {
    ArticleNotFound,
    BlogNotExist,
    ValidationError
};

export { errors };
