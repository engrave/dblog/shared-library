export class BlogNotExist extends Error {
    constructor() {
        super("Blog not exists");
    }
}
