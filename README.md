# Dependencies
You need to add following dependencies to your package.json file manually:
```
"dependencies": {
    "axios": "0.21.1",
    "pino": "8.14.1",
    "pino-pretty": "10.0.0",
    "express-pino-logger": "6.0.0",
    "steem-content-renderer": "2.0.2",
    "hivesigner": "3.1.3",
    "@hiveio/dhive": "1.2.0",
    "mongoose": "5.6.0",
    "ioredis": "4.10.0",
    "http-status-codes": "1.3.2",
    "express": "4.17.1",
    "express-validator": "5.3.1",
    "@sentry/node": "5.12.2"
},
"devDependencies": {
    "@types/express": "4.17.0",
    "@types/mongoose": "5.5.6",
    "ts-node": "8.3.0",
    "typescript": "5.1.6"
    "nodemon": "1.19.1"
}
```
