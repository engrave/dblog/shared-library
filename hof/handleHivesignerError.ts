import {logger} from '../utils/logger';

export async function handleHivesignerError(handler: any) {
    try {
        return await handler();
    } catch (error) {
        logger.error(error, 'Hivesigner error');

        if (error.error_description) {
            const description = error.error_description.split('\n')[0].split(': ')[1];

            if (description && description.length > 0) {
                throw new Error(description);
            }
            throw new Error('Unexpected Hivesigner error');
        } else {
            throw new Error(error.message);
        }
    }
}

