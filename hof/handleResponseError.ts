import {Request, Response} from 'express';
import {validationResult} from 'express-validator/check';
import * as HttpStatus from 'http-status-codes';
import {logger} from '../utils/logger';
import {ValidationError} from '../helpers/errors/ValidationError';

const Sentry = require('@sentry/node');

async function checkValidation(req: Request) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return Promise.reject({
            statusCode: HttpStatus.UNPROCESSABLE_ENTITY,
            body: {
                type: 'validation',
                validation: errors.array(),

                // deprecated
                error: errors.array()
            }
        });
    }
}

export async function handleResponseError(handler: any, req: Request, res: Response) {
    try {
        await checkValidation(req);
        return await handler(req, res);
    } catch (error) {
        if (error.response) {
            // The request was made and the server responded with a status code
            // that falls out of the range of 2xx
            logger.error({
                ...error.response.data,
                ...error.response.status,
                ...error.response.headers
            });
        } else if (error.request) {
            // The request was made but no response was received
            // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
            // http.ClientRequest in node.js
            logger.error(error.request);
        } else {
            // Something happened in setting up the request that triggered an Error
            logger.error(error);
        }

        if (error instanceof ValidationError) {
            return res.status(HttpStatus.UNPROCESSABLE_ENTITY).json({
                type: 'validation',
                validation: { message: error.message },

                // deprecated
                error: error.toString().replace(/"|[e|E]rror:/g, '')
            });
        }

        const {
            statusCode,
            body,
            response
        } = error;

        if (!statusCode){

            Sentry.captureException(error);

            return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
                type: 'internal',
                internal: {
                    message: 'Unexpected error encountered',
                    debug: error.toString().replace(/"|[e|E]rror:/g, ''),
                    stack: error.stack
                },

                // deprecated
                error: error.toString().replace(/"|[e|E]rror:/g, '')
            });
        }

        if (response) {
            return res.status(response.statusCode).json(response.body);
        } else {
            return res.status(statusCode).json(body);
        }

    }
}
