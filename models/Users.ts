import {Model, model, Schema} from 'mongoose';
import {IUser, UserScope} from '../interfaces/IUser';

export let UserSchema = new Schema(
    {
        username: {
            type: String,
            required: true,
            unique: true,
            index: true,
            trim: true
        },
        scope: {
            type: String,
            required: true,
            trim: true,
            default: UserScope.BLOG
        },
        imported: {
            type: Boolean,
            default: false
        },
        adopter: {
            type: Boolean,
            default: false
        },
        email: {
            type: String,
            unique: true,
            sparse: true,
            index: true
        },
        newsletter: {
            type: Boolean,
            default: false
        },
        created: Date,
        confirmation_token: String,
        confirmed: {
            type: Boolean,
            default: false
        },
        sandbox: {
            type: Boolean,
            default: false
        }
    },
    {timestamps: true}
);

export let Users: Model<IUser> = model<IUser>('users', UserSchema);
