import {Model, model, Schema} from 'mongoose';
import {IPublished} from '../interfaces/IPublished';

export let PublishedSchema: Schema = new Schema(
    {
        date: Date,
        title: String,
        abstract: String,
        image: String,
        username: String,
        steemit_permlink: {
            type: String,
            required: true,
            index: true
        },
        engrave_permlink: {
            type: String,
            required: true,
            index: true
        },
        content_category: {
            type: String,
            default: 'Other'
        },
        hidden: {
            type: Boolean,
            default: false
        }
    },
    {timestamps: true}
);

export const PublishedArticles: Model<IPublished> = model<IPublished>("published", PublishedSchema);
