import {Model, model, Schema} from 'mongoose';
import {IPost} from '../interfaces/IPost';

export let PostsSchema: Schema = new Schema(
    {
        blogId: {
            type: String,
            required: true,
            sparse: true,
            index: true
        },
        publishedAt: Date,
        username: {
            type: String,
            required: true,
            sparse: true,
            index: true
        },
        scheduledAt: {
            type: Date,
            required: false
        },
        title: String,
        body: String,
        categories: [String],
        tags: [String],
        featured_image: String,
        thumbnail_image: String,
        status: {
            type: String
        },
        decline_reward: {
            type: Boolean,
            default: false
        },
        permlink: {
            type: String,
            sparse: true,
            index: true
        },
        parent_category: String,
        hidden: {
            type: Boolean,
            default: false
        }
    },
    {timestamps: true}
);

export const Posts: Model<IPost> = model<IPost>('posts', PostsSchema);
