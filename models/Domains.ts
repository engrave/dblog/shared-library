import {Model, model, Schema} from 'mongoose';
import {IDomain} from '../interfaces/IDomain';

export let DomainSchema = new Schema(
    {
        owner: String,
        domain: String,
        blogId: String,
        creationDate: Date,
        expirationDate: Date,
        purchasePrice: {
            hive: Number,
            hbd: Number
        },
        paid: Boolean,
        payment: {
            transaction_id: String,
            block_num: Number,
            amount: String
        },
        logs: [
            {
                date: Date,
                msg: String
            }
        ]
    },
    {timestamps: true}
);

export let Domains: Model<IDomain> = model<IDomain>('domains', DomainSchema);
