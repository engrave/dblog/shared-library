import {Model, model, Schema} from 'mongoose';
import {IBlog} from '../interfaces/IBlog';
import {CollaborationType} from '../enums/CollaborationType';

export let BlogSchema = new Schema(
    {
        owner: String,
        collaboration_type: {
            type: String,
            default: CollaborationType.MANY_USERS
        },
        collaborators: [
            {
                _id: false,
                username: String,
                role: {type: String}
            }
        ],
        domain: {
            type: String,
            index: {
                unique: true,
                sparse: true
            }
        },
        custom_domain: {
            type: String,
            index: {
                unique: true,
                sparse: true
            }
        },
        domain_redirect: Boolean,
        title: String,
        slogan: String,
        logo_url: String,
        favicon_url: String,
        main_image: String,

        link_facebook: String,
        link_twitter: String,
        link_linkedin: String,
        link_instagram: String,

        opengraph_default_image_url: String,
        opengraph_default_description: String,

        onesignal_app_id: String,
        onesignal_api_key: String,
        onesignal_body_length: Number,
        onesignal_logo_url: String,

        analytics_gtag: String,
        webmastertools_id: String,

        categories: [],

        theme: {
            type: String,
            default: 'clean-blog'
        },

        premium: {
            type: Boolean,
            default: false
        },

        content_category: {
            type: String,
            default: 'Other'
        },

        hidden_commenters: {
            type: [String],
            default: []
        },

        sandbox: {
            type: Boolean,
            default: false
        }
    },
    {timestamps: true}
);

BlogSchema.pre('save', function(next) {
    this.set('categories', new Array())
    return next()
})

export let Blogs: Model<IBlog> = model<IBlog>('blogs', BlogSchema);
