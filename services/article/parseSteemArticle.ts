import {IArticle} from '../../interfaces/IArticle';
import renderHiveCommentBody from './renderHiveCommentBody';
import {ICategory} from '../../interfaces/ICategory';

const striptags = require('striptags');

export default async (hiveArticle: any, categories: ICategory[]): Promise<IArticle> => {

    const {
        author,
        title,
        permlink,
        created
    } = hiveArticle;

    const body = renderHiveCommentBody(hiveArticle.body);
    const thumbnail_image = prepareArticleThumbnail(hiveArticle);
    const featured_image = prepareArticleFeaturedImage(hiveArticle);
    const tags = prepareArticleTags(hiveArticle);
    const images = prepareArticleImages(hiveArticle);
    const value = prepareArticleValue(hiveArticle);

    return {
        title,
        permlink,
        author,
        created,
        thumbnail_image,
        featured_image,
        body,
        tags,
        images,
        votes_count: hiveArticle.active_votes.length,
        net_votes: hiveArticle.net_votes,
        total_votes: hiveArticle.active_votes.length,
        value,
        abstract: striptags(body.substr(0, 250)),
        categories,
        comments: hiveArticle.children,
        decline_payout: hiveArticle.max_accepted_payout == '0.000 HBD' ? true : false
    };
};

function prepareArticleThumbnail(hiveArticle: any): string | null {
    try {
        const metadata = typeof hiveArticle.json_metadata === 'string' ? JSON.parse(hiveArticle.json_metadata) : hiveArticle.json_metadata;
        if (metadata.engrave?.featured_image) {
            return metadata.engrave.featured_image;
        }
        if (metadata.image && metadata.image[0]) {
            return metadata.image[0];
        }
        return null;
    } catch (error) {
        return null;
    }
}

function prepareArticleFeaturedImage(hiveArticle: any): string | null {
    try {
        const metadata = JSON.parse(hiveArticle.json_metadata);
        if (metadata.engrave?.featured_image) {
            return metadata.engrave.featured_image;
        }
        return null;
    } catch (error) {
        return null;
    }
}

function prepareArticleTags(hiveArticle: any): string[] {
    try {
        const metadata = JSON.parse(hiveArticle.json_metadata);
        if (metadata.tags) {
            return metadata.tags;
        }
        return [];
    } catch (error) {
        return [];
    }
}

function prepareArticleImages(hiveArticle: any): string[] | null {
    try {
        const metadata = JSON.parse(hiveArticle.json_metadata);
        if (metadata.image) {
            return metadata.image;
        }
        return null;
    } catch (error) {
        return null;
    }
}

function prepareArticleValue(hiveArticle: any): number {
    const pendingPayout = parseFloat(hiveArticle.pending_payout_value.replace(' HBD', ''));
    const curatorPayout = parseFloat(hiveArticle.curator_payout_value.replace(' HBD', ''));
    const authorPayout = parseFloat(hiveArticle.author_payout_value.replace(' HBD', ''));

    return pendingPayout + curatorPayout + authorPayout;
}
