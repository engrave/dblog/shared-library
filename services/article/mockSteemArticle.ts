const mockSteemArticle = (username: string, title: string, body: string, permlink: string, tags: string[]) => {
    return {
        id: 76495272,
        author: username,
        permlink,
        category: tags[0],
        parent_author: '',
        parent_permlink: tags[0],
        title,
        body,
        json_metadata: '{}',
        last_update: '2019-06-16T10:19:39',
        created: '2019-06-16T10:19:39',
        active: '2019-06-16T10:19:39',
        last_payout: '1970-01-01T00:00:00',
        depth: 0,
        children: 0,
        net_rshares: 0,
        abs_rshares: 0,
        vote_rshares: 0,
        children_abs_rshares: 0,
        cashout_time: '2019-06-23T10:19:39',
        max_cashout_time: '1969-12-31T23:59:59',
        total_vote_weight: 0,
        reward_weight: 10000,
        author_payout_value: '0.000 HBD',
        curator_payout_value: '0.000 HBD',
        author_rewards: 0,
        net_votes: 0,
        root_author: username,
        root_permlink: permlink,
        max_accepted_payout: '0.000 HBD',
        percent_steem_dollars: 10000,
        allow_replies: true,
        allow_votes: true,
        allow_curation_rewards: true,
        beneficiaries: [
           {
              account: 'engrave',
              weight: 1500
           }
        ],
        url: `/${tags[0]}/@${username}/${permlink}`,
        root_title: 'Just a test',
        pending_payout_value: '0.000 HBD',
        total_pending_payout_value: '0.000 HIVE',
        active_votes: <any> [],
        replies: <any> [],
        author_reputation: '48342168333',
        promoted: '0.000 HIVE',
        body_length: 0,
        reblogged_by: <any> []
     };
};

export { mockSteemArticle };
