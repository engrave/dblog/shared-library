import {DefaultRenderer} from 'steem-content-renderer';
import removeDappsInfo from '../../utils/removeDappsInfo';

const renderer = new DefaultRenderer({
    baseUrl: 'https://peakd.com/',
    breaks: true,
    skipSanitization: false,
    addNofollowToLinks: true,
    doNotShowImages: false,
    ipfsPrefix: '',
    assetsWidth: 640,
    assetsHeight: 480,
    imageProxyFn: (url: string): string => url,
    usertagUrlFn: (account: string): string => `https://peakd.com/@${account}`,
    hashtagUrlFn: (hashtag: string): string => `https://peakd.com/trending/${hashtag}`,
    isLinkSafeFn: (_url: string): boolean => true
});

export default function renderHiveCommentBody(body: string): string {
    let rendered = body;

    rendered = removeDappsInfo(rendered);
    rendered = `<html>${renderer.render(rendered)}</html>`;

    return rendered;
}
