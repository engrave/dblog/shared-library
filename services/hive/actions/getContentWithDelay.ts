import {client} from './engine/engine';
import {Repeater} from './Repeater';

async function getContentWithDelay(username: string, permlink: string): Promise<any> {
    try {
        // tslint:disable-next-line:no-magic-numbers
        return new Repeater(30).execute(async () => client.call('bridge', 'get_post', {author: username, permlink}));
    } catch (error) {
        return null;
    }
}

export default getContentWithDelay;
