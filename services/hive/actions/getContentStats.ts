import {client} from './engine/engine';

async function getContentStats(username: string, permlink: string): Promise<{net_votes: number; value: number}> {
    try {
        const article = await client.call('bridge', 'get_post', {author: username, permlink});
        return {
            net_votes: article.net_votes,
            value: prepareValue(article)
        };
    } catch (error) {
        return {
            net_votes: 0,
            value: 0
        };
    }
}

function prepareValue(article: any): number {
    const pendingPayout = parseFloat(article.pending_payout_value.replace(' HBD', ''));
    const curatorPayout = parseFloat(article.curator_payout_value.replace(' HBD', ''));
    const authorPayout = parseFloat(article.author_payout_value.replace(' HBD', ''));

    return pendingPayout + curatorPayout + authorPayout;
}

export default getContentStats;
