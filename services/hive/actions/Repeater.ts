import {logger} from '../../../utils';

export class Repeater {
    private readonly repeat: number = 5;
    private readonly interval: number = 1000;
    private count: number = 0;

    constructor(repeat: number = 5) {
        this.repeat = repeat;
        this.count = 0;
    }

    public async execute(handler: any): Promise<any> {
        try {
            return await handler();
        } catch (e) {
            this.count += 1;
            if (this.count < this.repeat) {
                logger.error(e);
                await this.delay();
                return this.execute(handler);
            }
            throw e;
        }
    }

    private async delay(): Promise<void> {
        return new Promise((resolve) => {
            return setTimeout(resolve, this.interval);
        });
    }
}
