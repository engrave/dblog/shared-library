import {client} from './engine/engine';
import {logger} from '../../../utils';

async function getHiveArticle(author: string, permlink: string): Promise<any> {
    try {
        const article = await client.call('bridge', 'get_post', {author, permlink});

        if (typeof article === 'string') {
            throw new Error('Article not valid');
        }

        if (!article || article.author === '') {
            throw new Error('Article not valid');
        }

        return article;
    } catch (error) {
        logger.error(error);
        return null;
    }
}

export default getHiveArticle;
