import {blockchain} from '../../../../config';
import {Client} from '@hiveio/dhive';

const client = new Client(blockchain.nodes);

export {
    client
};
