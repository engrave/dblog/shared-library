import engine from '../../store/engine';
import {IBlog} from '../../../../interfaces/IBlog';
import keys from '../../store/keys';

const JSONCache = require('redis-json');
const blogs = new JSONCache(engine, {prefix: `${keys.blogs}:`});

async function setBlog(blog: IBlog): Promise<IBlog>{

    const flattened = JSON.parse(JSON.stringify(blog)) ;

    await blogs.rewrite(blog.domain, flattened);
    if(blog.custom_domain) {
        await blogs.rewrite(blog.custom_domain, flattened);
    }

    return blog;
}

export default setBlog;
