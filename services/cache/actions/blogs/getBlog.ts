import {setBlog} from '../../cache';
import engine from '../../store/engine';
import {Blogs} from '../../../../models/Blogs';
import {IBlog} from '../../../../interfaces/IBlog';
import keys from '../../store/keys';
import {BlogNotExist} from '../../../../helpers/errors/BlogNotExist';

const JSONCache = require('redis-json');
const blogs = new JSONCache(engine, {prefix: `${keys.blogs}:`});

async function getBlog(hostname: string): Promise<IBlog> {
    try {
        const blog = await blogs.get(hostname);

        if( ! blog || blog.removed) {

            const dbBlog = await Blogs.findOne({domain: hostname});

            if( ! dbBlog) throw new BlogNotExist();

            return await setBlog(dbBlog);

        }

        return normalizeCachedObject(blog);

    } catch (error) {
        throw new BlogNotExist();
    }
}

const normalizeCachedObject = (blog: any): IBlog => {
    return {
        ...blog,
        domain_redirect: toBoolean(blog.domain_redirect),
        premium: toBoolean(blog.premium),
        sandbox: toBoolean(blog.sandbox)

    }
}

const toBoolean = (str: string): boolean => str === 'true';

export default getBlog;
