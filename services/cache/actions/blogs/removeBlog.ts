import engine from '../../store/engine';
import keys from '../../store/keys';
import {IBlog} from '../../../../interfaces/IBlog';

const JSONCache = require('redis-json');
const blogs = new JSONCache(engine, {prefix: `${keys.blogs}:`});

async function removeBlog(blog: IBlog): Promise<void> {

    await blogs.rewrite(blog.domain, {removed: true});
    if (blog.custom_domain) {
        await blogs.rewrite(blog.custom_domain, {removed: true});
    }

}

export default removeBlog;
