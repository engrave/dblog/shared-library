import engine from '../../store/engine';
import keys from '../../store/keys';

async function votedForWitness(username: string): Promise<boolean> {
    return (await engine.get(`${keys.witnessVotes}:${username}`)) != null;
}

export default votedForWitness;
