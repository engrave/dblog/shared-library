import engine from '../../store/engine';
import keys from '../../store/keys';

async function setVotedForWitness(username: string) {
    return await engine.set(`${keys.witnessVotes}:${username}`, true);
}

export default setVotedForWitness;
