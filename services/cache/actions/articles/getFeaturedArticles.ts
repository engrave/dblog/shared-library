import {IArticle} from '../../../../interfaces/IArticle';
import engine from '../../store/engine';
import keys from '../../store/keys';

async function getFeaturedArticles(blogId: string, skip: number, limit: number): Promise<IArticle[]> {
    let permlinks: any = [];

    const now = new Date().getTime();
    const offset = 6 * 24 * 60 * 60 * 100; // 7 days
    const offsetTimestamp = now - offset;

    const articlesInLastSevenDays = await engine.zcount(`${keys.blogCreatedList}:${blogId}`, offsetTimestamp, now);

    if (articlesInLastSevenDays <= limit) {
        permlinks = await engine.zrevrange(`${keys.blogCreatedList}:${blogId}`, skip, limit ? skip + limit : skip + 11);
    } else {
        permlinks = await engine.zrangebyscore(`${keys.blogCreatedList}:${blogId}`, offsetTimestamp, now);
    }

    if (!permlinks.length) {
        return [];
    }

    const rawPosts = await engine.mget(permlinks);

    const posts = rawPosts.map((post: any) => JSON.parse(post)).sort((a: any, b: any) => b.value - a.value);

    return posts.slice(0, limit);
}

export default getFeaturedArticles;
