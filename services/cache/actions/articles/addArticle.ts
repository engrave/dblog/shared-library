import setArticleContent from './setArticleContent';
import {setArticleExist} from '../../cache';
import updateArticleCategories from '../categories/updateArticleCategories';
import {ICategory} from '../../../../interfaces/ICategory';
import {IArticle} from '../../../../interfaces/IArticle';

// tslint:disable-next-line:variable-name
async function addArticle(blogId: string, permlink: string, hiveArticle: any, categories: ICategory[]): Promise<IArticle> {

    const timestamp = (new Date(hiveArticle.created)).getTime();

    await setArticleExist(blogId, hiveArticle.author, permlink);
    await updateArticleCategories(blogId, permlink, timestamp, categories);
    return setArticleContent(hiveArticle.author, permlink, hiveArticle);

}

export default addArticle;
