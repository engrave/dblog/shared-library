import engine from '../../store/engine';
import getUsernameFromPermlink from '../articles/getUsernameFromPermlink';
import removeFromAllCategories from '../categories/removeArticleFromAllCategories';
import keys from '../../store/keys';
import {logger} from '../../../../utils/logger';
import {setArticleExist} from '../../cache';
import updateArticleCategories from '../categories/updateArticleCategories';
import {ICategory} from '../../../../interfaces/ICategory';

async function moveToBlog(blogId: string, destBlogId: string, category: ICategory, permlink: string): Promise<void> {
    try {
        const username = await getUsernameFromPermlink(blogId, permlink);
        const cachedArticle = await engine.get(`${keys.cachedArticles}:${username}:${permlink}`);
        const article = JSON.parse(cachedArticle);

        if (article) {
            await removeFromAllCategories(blogId, permlink);
        }

        const timestamp = new Date(article.created).getTime();

        await setArticleExist(destBlogId, username, permlink);
        await updateArticleCategories(destBlogId, permlink, timestamp, [category]);
        await engine.set(`${keys.cachedArticles}:${username}:${permlink}`, JSON.stringify({...article, categories: [category]}));
    } catch (error) {
        logger.error(error);
    }
}

export default moveToBlog;
