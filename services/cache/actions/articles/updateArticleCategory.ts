import engine from '../../store/engine';
import keys from '../../store/keys';

async function updateArticleCategory(blogId: string, permlink: string, categoryObject: any) {
    const articleKey = await engine.get(`${keys.whichUsername}:${blogId}:${permlink}`);
    const articleFromCache = await engine.get(articleKey);

    const article = JSON.parse(articleFromCache);

    for (let category in article.categories) {
        if (article.categories[category]._id === categoryObject._id.toString()) {
            article.categories[category] = categoryObject;
        }
    }

    return await engine.set(`${keys.cachedArticles}:${article.author}:${permlink}`, JSON.stringify(article));
}

export default updateArticleCategory;
