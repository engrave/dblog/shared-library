import getArticleCategories from './getArticleCategories';
import getUsernameFromPermlink from '../articles/getUsernameFromPermlink';
import engine from '../../store/engine';
import keys from '../../store/keys';
import {logger} from '../../../../utils/logger';

const removeFromAllCategories = async (blogId: string, permlink: string) => {
    try {
        const username = await getUsernameFromPermlink(blogId, permlink);
        const categories = await getArticleCategories(blogId, permlink);
        const key = `${keys.cachedArticles}:${username}:${permlink}`;

        if (categories) {
            for (const category of categories) {
                await engine.zrem(`${keys.blogCategoryList}:${blogId}:${category._id}`, key);
            }
        }

        await engine.zrem(`${keys.blogCreatedList}:${blogId}`, `${keys.cachedArticles}:${username}:${permlink}`);
    } catch (error) {
        logger.error(error);
    }
};

export default removeFromAllCategories;
