export default {
    blogs: 'blogs',
    users: 'users',
    articleExist: 'exist',
    whichUsername: 'posts',
    whichBlogId: `whichblogid`,
    cachedArticles: 'article',
    articleWhichCategories: 'categories',
    blogCreatedList: 'created',
    blogCategoryList: 'category',
    witnessVotes: 'witness'
}
