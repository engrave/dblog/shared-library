import sc from '../hivesigner.service';
import {handleHivesignerError} from '../../../hof/handleHivesignerError';

export default async (access_token: string, parentAuthor: string, parentPermlink: string, author: string, permlink: string, title: string, body: string) => {
    return handleHivesignerError(async () => {
        const json_metadata = {app: 'engrave'};

        sc.blog.setAccessToken(access_token);

        return sc.blog.comment(parentAuthor, parentPermlink, author, permlink, title, body, json_metadata);
    });
};
