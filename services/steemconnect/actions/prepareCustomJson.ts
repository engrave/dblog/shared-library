const prepareCustomJson = (author: string, json: object): object => {
    return {
        id: 'engrave',
        from: author,
        required_auths: [],
        required_posting_auths: [author],
        json: JSON.stringify(json)
    }
}

export default prepareCustomJson;
