import sc from '../hivesigner.service';
import {handleHivesignerError} from '../../../hof/handleHivesignerError';

const STEEMIT_MAX_PERMLINK_LENGTH = 255;

export default async (access_token: string, author: string, title: string, body: string, parentAuthor: string, parentPermlink: string) => {

    return handleHivesignerError( async () => {

        const commentPermlink = generateCommentPermlink(parentAuthor, parentPermlink);
        const json_metadata = { app: 'engrave' };
        sc.blog.setAccessToken(access_token);
        return sc.blog.comment(parentAuthor, parentPermlink, author, commentPermlink, title, body, json_metadata);

    });
};

const generateCommentPermlink = (parentAuthor: string, parentPermlink: string) => {
    const timeStr = new Date() .toISOString().replace(/[^a-zA-Z0-9]+/g, "").toLowerCase();
    const nParentPermlink = parentPermlink.replace(/(-\d{8}t\d{9}z)/g, "");
    let permLink = `re-${parentAuthor}-${nParentPermlink}-${timeStr}`;
    if (permLink.length > STEEMIT_MAX_PERMLINK_LENGTH) {
        permLink.substr(permLink.length - STEEMIT_MAX_PERMLINK_LENGTH, permLink.length);
    }
    return permLink.toLowerCase().replace(/[^a-z0-9-]+/g, "");
}
