const secrets = require('@cloudreach/docker-secrets');
import axios, {AxiosRequestConfig} from 'axios';
import sc from '../hivesigner.service';

export default async (refresh_token: string) => {
    const client_secret = secrets.SC2_APP_SECRET;

    const options: AxiosRequestConfig = {
        method: 'POST',
        data: {
            refresh_token: refresh_token,
            client_id: process.env.STEEMCONNECT_ID,
            client_secret: client_secret,
            scope: sc.dashboard.scope
        },
        url: 'https://hivesigner.com/api/oauth2/token'
    };

    return await axios(options);
};
