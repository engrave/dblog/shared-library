import sc from '../hivesigner.service';
import {handleHivesignerError} from '../../../hof/handleHivesignerError';
import prepareCustomJson from './prepareCustomJson';

export default async (accessToken: string, voter: string, author: string, permlink: string, weight: number) => {
    const json = {
        type: 'vote',
        payload: {
            voter,
            author,
            permlink,
            weight
        }
    };

    const operations: any[] = [
        ['vote', {voter, author, permlink, weight}],
        ['custom_json', prepareCustomJson(voter, json)]
    ];

    return await handleHivesignerError(async () => {
        sc.blog.setAccessToken(accessToken);
        return await sc.blog.broadcast(operations);
    });
};
