import sc from '../hivesigner.service';
import {handleHivesignerError} from '../../../hof/handleHivesignerError';
import {logger} from '../../../utils';

// tslint:disable-next-line:variable-name
export default async (access_token: string, username: string): Promise<any> => {

    return handleHivesignerError( async () => {

        sc.blog.setAccessToken(access_token);
        const { account } = await sc.blog.me();
        logger.info(account);
        return sc.blog.claimRewardBalance(username, account.reward_steem_balance, account.reward_sbd_balance, account.reward_vesting_balance);

    });
};
