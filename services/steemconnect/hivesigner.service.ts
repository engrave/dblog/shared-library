const hivesigner = require('hivesigner');

import getRefreshToken from './actions/getRefreshToken';
import fetchElevatedAccessToken from './actions/fetchElevatedAccessToken';
import vote from './actions/vote';
import comment from './actions/comment';
import editComment from './actions/editComment';
import claimRewards from './actions/claimRewards';

const dashboard = new hivesigner.Client({
    app: process.env.STEEMCONNECT_ID,
    callbackURL: process.env.SC2_REDIRECT_DASHBOARD,
    scope: ['offline', 'login', 'vote', 'comment', 'delete_comment', 'comment_options', 'claim_reward_balance', 'account_update']
});

const blog = new hivesigner.Client({
    app: process.env.STEEMCONNECT_ID,
    callbackURL: process.env.SC2_REDIRECT_BLOG,
    scope: ['login', 'vote', 'comment', 'comment_options', 'custom_json']
});

const sc = {
    dashboard,
    blog,
    getRefreshToken,
    fetchElevatedAccessToken,
    vote,
    comment,
    editComment,
    claimRewards
};

export default sc;
