import {Document} from 'mongoose';

export enum UserScope {
    BLOG = 'blog',
    DASHBBOARD = 'dashboard'
}

export interface IUser extends Document {
    username: string,
    scope: UserScope,
    adopter: boolean,
    imported: boolean,
    email: string,
    newsletter: boolean,
    created: Date,
    confirmation_token: string,
    confirmed: boolean
}
