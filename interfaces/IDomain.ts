import {Document} from 'mongoose';

export interface IDomain extends Document {
    owner: string;
    domain: string;
    blogId: string;
    creationDate: Date;
    expirationDate: Date;
    purchasePrice: {
        hive: number;
        hbd: number;
    };
    paid: boolean;
    payment: {
        transaction_id: string;
        block_num: number;
        amount: string;
    };
    logs: [
        {
            date: Date;
            msg: string;
        }
    ];
}
