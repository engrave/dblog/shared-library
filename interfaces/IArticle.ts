import {ICategory} from './ICategory';

export interface IArticle {
    author: string;
    title: string;
    permlink: string;
    body: string;
    abstract: string;
    created: Date;
    tags: string[];
    featured_image: string;
    thumbnail_image: string;
    votes_count: number;
    value: number;
    comments: number;
    categories: ICategory[];
    images?: string[];
    [optional: string]: any;
}
