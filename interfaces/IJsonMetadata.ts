export interface IJsonMetadata {
    format: string,
    app: string,
    users?: string[],
    image?: string[],
    tags?: string[],
    links?: string[],
}