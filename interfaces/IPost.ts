import {PostStatus} from '../enums/PostStatus';
import {Document} from 'mongoose';

export interface IPost extends Document {
    blogId: string;
    publishedAt: Date;
    username: string;
    scheduledAt: Date;
    createdAt: Date;
    updatedAt: Date;
    title: string;
    body: string;
    categories: [string];
    tags: [string];
    featured_image: string;
    thumbnail_image: string;
    status: PostStatus;
    decline_reward: Boolean;
    permlink: string;
    parent_category: string;
    hidden: boolean;
}
