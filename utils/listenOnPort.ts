import {Application} from 'express';
import {logger} from './logger';

const signals: NodeJS.Signals[] = ['SIGINT', 'SIGTERM'];

export function listenOnPort(app: Application, port: number) {

    const server = app.listen(port);

    server.on('error', (error: Error) => {
        logger.error(error);
    });

    server.on('listening', () => {
        logger.info(`Listening on port ${port}`);
    });

    signals.forEach((signal: NodeJS.Signals) => {
        process.on(signal, () => {
            logger.info(`Shutting down because of ${signal}`);
            server.close(() => {
                logger.warn('Server closed gracefully');
            });
        });
    });

    process.on('unhandledRejection', (reason) => {
        logger.fatal(reason, 'Unhandled Rejection');
        process.exit(1);
    });
}
