import {PrettyOptions} from 'pino-pretty';

const pino = require('pino');
const pinoExpress = require('express-pino-logger');

const prettyPrintOptions: PrettyOptions = {
    colorize: true,
    translateTime: true,
    levelFirst: true,
    ignore: 'v',
    hideObject: false
};

const logger = pino({
    prettyPrint: process.env.NODE_ENV === 'development' ? prettyPrintOptions : false
});

const endpointLogger = pinoExpress({
    prettyPrint: process.env.NODE_ENV === 'development' ? prettyPrintOptions : false
});

export {logger, endpointLogger};
