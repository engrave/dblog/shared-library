import {IBlog} from '../interfaces/IBlog';

const appendEngraveInfo = (body: string, permlink: string, blog: IBlog): string => {
    switch (process.env.TARGET) {
        case 'dblog':
            return `${body}\n\n***\n<center><sup>Pierwotnie opublikowano na [${blog.title}](https://${
                blog.custom_domain || blog.domain
            }/${permlink}). Blog na Hive napędzany przez [dBlog](https://dblog.pl).</sup></center>`;
        default:
            return `${body}\n\n***\n<center><sup>Originally posted on [${blog.title}](http://${
                blog.custom_domain || blog.domain
            }/${permlink}). Hive blog powered by [ENGRAVE](https://dblog.org).</sup></center>`;
    }
};

export default appendEngraveInfo;
