import * as HttpStatus from 'http-status-codes';

const customValidatorError = async (location: string, param: string, value: string, msg: string) => {
    return Promise.reject({
        statusCode: HttpStatus.UNPROCESSABLE_ENTITY,
        body: {
            type: 'validation',
            validation: {
                location: location,
                param: param,
                value: value,
                msg: msg
            }
        },
        // deprecated
        error: {
            location: location,
            param: param,
            value: value,
            msg: msg
        }
    });
};

export { customValidatorError };
