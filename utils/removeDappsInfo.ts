
function removeDappsInfo(body: string): string {
    let filtered = body;

    filtered = removeEngraveInfo(filtered);
    filtered = removePartikoInfo(filtered);

    return filtered;
}

function removeEngraveInfo(body: string): string {

    const result = body
        .replace(/(\*\*\*\*\*\*\*\*\*\*\*\n\nArtykuł autorstwa: @)(?:.*)(, dodany za pomocą serwisu )(?:.*)\(https:\/\/(?:.*)\)/g, "")

        .replace(/(\n\*\*\*\n\n###\sOriginally posted on \[)(.*)(\)\.\sSteem blog powered by \[)(.*)(\)\.)/g, "")
        .replace(/(\n\*\*\*\n\s###\sPierwotnie opublikowano na \[)(.*)(\)\.\sBlog na Steem napędzany przez \[)(.*)(\)\.)/g, "")
        .replace(/(\n\*\*\*\n\n###\sOryginally posted on \[)(.*)(\)\.\sSteem blog powered by \[)(.*)(\)\.)/g, "")

        .replace(/(\n\*\*\*\n<center><sup>Originally posted on \[)(.*)(\)\.\sSteem blog powered by \[)(.*)(\)\.<\/sup><\/center>)/g, "")
        .replace(/(\n\*\*\*\n<center><sup>Pierwotnie opublikowano na \[)(.*)(\)\.\sBlog na Steem napędzany przez \[)(.*)(\)\.<\/sup><\/center>)/g, "")

        .replace(/(\n\*\*\*\n\n###\sOriginally posted on \[)(.*)(\)\.\sHive blog powered by \[)(.*)(\)\.)/g, "")
        .replace(/(\n\*\*\*\n\s###\sPierwotnie opublikowano na \[)(.*)(\)\.\sBlog na Hive napędzany przez \[)(.*)(\)\.)/g, "")
        .replace(/(\n\*\*\*\n\n###\sOryginally posted on \[)(.*)(\)\.\sHive blog powered by \[)(.*)(\)\.)/g, "")

        .replace(/(\n\*\*\*\n<center><sup>Originally posted on \[)(.*)(\)\.\sHive blog powered by \[)(.*)(\)\.<\/sup><\/center>)/g, "")
        .replace(/(\n\*\*\*\n<center><sup>Pierwotnie opublikowano na \[)(.*)(\)\.\sBlog na Hive napędzany przez \[)(.*)(\)\.<\/sup><\/center>)/g, "");

    return result;
}

function removePartikoInfo(body: string): string {
    return body
        .replace('Posted using [Partiko Android](https://steemit.com/@partiko-android)', '')
        .replace('Posted using [Partiko iOS](https://steemit.com/@partiko-ios)','')
        .replace(/Posted using \[Partiko Android\]\(https:\/\/partiko.app\/referral\/.*\)/, '')
        .replace(/Posted using \[Partiko iOS\]\(https:\/\/partiko.app\/referral\/.*\)/, '');
}

export default removeDappsInfo;
