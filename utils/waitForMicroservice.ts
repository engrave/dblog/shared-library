import axios, {AxiosRequestConfig} from 'axios';
import {logger} from './logger';

const DELAY_MS = 1000;
const TIMEOUT_MS = process.env.NODE_ENV === 'production' ? 30000 : 30000000;

const getCurrentTimestamp = (): number => new Date().getTime();
const delay = (milliseconds: number): Promise<void> => new Promise((resolve): void => {
    setTimeout(resolve, milliseconds);
});

const waitForMicroservice = async (path: string): Promise<void> => {

    const startedAt = getCurrentTimestamp();
    let isAlive = false;
    do {
        logger.info(`Waiting for: ${path}`);
        isAlive = await ping(path);
        if (!isAlive) { await delay(DELAY_MS); }
    } while (!isAlive && getCurrentTimestamp() - startedAt < TIMEOUT_MS);

    if (!isAlive) {
        throw new Error(`Service ${path} not responded after ${TIMEOUT_MS / 1000} seconds`);
    } else {
        logger.info(`Service ${path} is now available`);
    }
};

async function ping(path: string): Promise<boolean> {

    try {
        const options: AxiosRequestConfig = {
            method: 'GET',
            url: `http://${path}/health/ping`
        };

        const response = await axios(options);

        const {data: {message}} = response;

        return message === 'pong';

    } catch (error) {
        return false;
    }
}

export {waitForMicroservice};
