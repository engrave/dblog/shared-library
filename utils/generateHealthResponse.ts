import * as os from 'os';

export function generateHealthResponse() {
    return {
        message: 'pong',
        version: process.env.npm_package_version,
        name: process.env.npm_package_name,
        instance: os.hostname(),
        uptime: process.uptime()
    };
}
