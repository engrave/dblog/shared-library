const options = {
    useCreateIndex: true,
    useNewUrlParser: true, 
    replicaSet: process.env.MONGO_URI ? null : 'replica-set'
};

const uri = process.env.MONGO_URI || "mongodb://mongo-primary:27017,mongo-worker-one:27017,mongo-worker-two:27017/engrave";

export { 
    options,
    uri
 };