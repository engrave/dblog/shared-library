const nodes = process.env.API_NODES
    ? process.env.API_NODES.split(' ')
    : ['https://api.openhive.network', 'https://anyx.io', 'https://api.deathwing.me'];

const blockchain = {
    nodes,
    node: nodes[0]
};

export { blockchain };
