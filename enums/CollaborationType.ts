export enum CollaborationType {
    MANY_USERS = "many",
    CONSOLIDATE = 'consolidate'
}