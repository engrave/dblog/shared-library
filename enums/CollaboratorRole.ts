export enum CollaboratorRole {
    ADMIN = "admin",
    MODERATOR = "moderator",
    EDITOR = "editor"
}