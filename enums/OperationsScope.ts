export enum OperationsScope {
    PUBLISH = "publish",
    EDIT = "edit",
    REMOVE = 'remove'
}