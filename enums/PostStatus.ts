export enum PostStatus {
    DRAFT = "draft",
    ACCEPTED = 'accepted',
    REJECTED = 'rejected',
    PUBLISHED = 'published'
}